#! /usr/bin/python3
import time

from rest_client import RestClient
import json


def test_add_first_bear(task_rest):
    rest = RestClient()
    request_data = {"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}
    data, code, text = rest.send_post('bear', request_data)
    assert code == 200
    assert data == 1

    request_data['bear_id'] = data
    data, code, text = rest.send_get('bear', True)
    assert code == 200
    assert data == [request_data]


def test_add_bear_empty(task_rest):
    rest = RestClient()
    request_data = {"bear_type":"BLACK","bear_age":17.5}
    data, code, text = rest.send_post('bear', request_data, True)
    assert code == 400


def test_add_bear_negative_age(task_rest):
    rest = RestClient()
    request_data = {"bear_type":"BLACK", "bear_name":"MIKHAIL", "bear_age":-1}
    data, code, text = rest.send_post('bear', request_data, True)
    assert code == 400

    data, code, text = rest.send_get('bear', True)
    assert code == 200
    assert data == []

def test_get_bears_from_empty_db(task_rest):
    rest = RestClient()
    data, code, text = rest.send_get('bear',True)
    assert code == 200
    assert data == []


def test_delete_bear(task_rest):
    rest = RestClient()
    request_data = {"bear_type": "BLACK", "bear_name": "mikhail", "bear_age": 17.5}
    data, code, text = rest.send_post('bear', request_data)
    assert code == 200
    assert data == 1

    data, code, text = rest.send_delete('bear/{}'.format(data), request_data, True)
    assert code == 200

    data, code, text = rest.send_get('bear', True)
    assert code == 200
    assert data == []


def test_change_bear(task_rest):
    rest = RestClient()
    request_data = {"bear_type": "BLACK", "bear_name": "MIKHAIL", "bear_age": 17.5}
    data, code, text = rest.send_post('bear', request_data)
    assert code == 200
    assert data == 1

    request_data = {"bear_type": "BLACK", "bear_name": "MIKHAIL", "bear_age": 50}
    data, code, text = rest.send_put('bear/1', request_data, True)
    assert code == 200

    request_data['bear_id'] = 1
    data, code, text = rest.send_get('bear', True)
    assert code == 200
    assert data == [request_data]