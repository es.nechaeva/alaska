#! /usr/bin/python3

import requests
import json
import time


class RestClient:
    url = "http://localhost:80/"

    def send_get(self, test_url, verbose=False):
        test_url = self.url + test_url
        json_data = None
        if verbose:
            print(test_url)
        response = requests.get(test_url)
        if response.text:
            if verbose:
                print(response.text)
            try:
                json_data = response.json()
            except:
                json_data = None
        return json_data, response.status_code, response.text

    def send_put(self, test_url, test_body, verbose=False):
        test_url = self.url + test_url
        json_data = ''
        if verbose:
            print(test_url)
        response = requests.put(test_url, data=json.dumps(test_body))
        if verbose:
            print(response)
        if verbose:
            print(response.text)
        if response.text:
            try:
                json_data = response.json()
            except:
                json_data = None
        return json_data, response.status_code, response.text

    def send_post(self, test_url, test_body, verbose=False):
        test_url = self.url + test_url
        json_data = None
        if verbose:
            print(test_url)
        response = requests.post(test_url, data=json.dumps(test_body))
        if verbose:
            print(response)
            print(response.text)
        if response.text:
            try:
                json_data = response.json()
            except:
                json_data = None
        return json_data, response.status_code, response.text

    def send_delete(self, test_url, test_body, verbose=False):
        test_url = self.url + test_url
        json_data = None
        if verbose:
            print(test_url)
        response = requests.delete(test_url, data=json.dumps(test_body))
        if verbose:
            print(response.text)
        if response.text:
            try:
                json_data = response.json()
            except:
                json_data = None
        return json_data, response.status_code, response.text

    def wait_connect(self, timeout=10):
        test_url = self.url + 'info'
        connect = False
        code = -1
        tm = time.time()
        while not connect and time.time() - tm < timeout:
            try:
                response = requests.get(test_url)
                code = response.status_code
            except:
                time.sleep(0.5)
        return code == 200

