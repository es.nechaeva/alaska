import time

import pytest
import docker
from rest_client import RestClient


@pytest.fixture()
def task_docker():
    # SetUp: start docker container
    client = docker.from_env()
    cont = client.containers.run('azshoo/alaska:1.0', detach=True, ports={'8091/tcp': 80})

    yield  # goto test

    # Teardown : stop docker container
    cont.stop()


@pytest.fixture()
def task_rest(task_docker):
    rest = RestClient()
    assert rest.wait_connect(timeout=10)
